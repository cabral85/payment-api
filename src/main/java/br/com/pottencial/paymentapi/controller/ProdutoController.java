package br.com.pottencial.paymentapi.controller;

import br.com.pottencial.paymentapi.model.Produto;
import br.com.pottencial.paymentapi.service.ProdutoService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping("produto")
public class ProdutoController {

    private final ProdutoService produtoService;

    @GetMapping
    public ResponseEntity buscaProdutos() {
        return ResponseEntity.status(HttpStatus.OK).body(produtoService.listaProdutos());
    }

    @GetMapping("{id}")
    public ResponseEntity buscaProdutoPorId(@PathVariable("id") Long idProduto) {
        Optional<Produto> produtoOptional = produtoService.buscaProdutoPorId(idProduto);
        if (produtoOptional.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(produtoOptional.get());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PostMapping
    public ResponseEntity salvaProduto(@RequestBody Produto produto) {
        if (produtoService.salvaProduto(produto)) {
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    @PutMapping("{id}")
    public ResponseEntity atualizaProduto(@PathVariable("id") Long idProduto, @RequestBody Produto produto) {
        if (produtoService.atualizaProduto(idProduto, produto)) {
            return ResponseEntity.status(HttpStatus.ACCEPTED).build();
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

}
