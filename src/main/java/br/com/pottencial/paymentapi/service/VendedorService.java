package br.com.pottencial.paymentapi.service;

import br.com.pottencial.paymentapi.model.Vendedor;
import br.com.pottencial.paymentapi.repository.VendedorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class VendedorService {
    private final VendedorRepository vendedorRepository;

    public boolean salvaVendedor(Vendedor vendedor) {
        return vendedorRepository.save(vendedor).getIdVendedor() != null;
    }

    public boolean atualizaVendedor(Long idVendedor, Vendedor vendedor) {
        Optional<Vendedor> vendedorOptional = vendedorRepository.findById(idVendedor);
        if (vendedorOptional.isPresent()) {
            Vendedor vendedorAtual = vendedorOptional.get();
            BeanUtils.copyProperties(vendedor, vendedorAtual, "idVendedor");
            vendedorRepository.save(vendedorAtual);
            return true;
        }
        return false;
    }

    public Optional<Vendedor> buscaVendedorPorId(Long idVendedor) {
        return vendedorRepository.findById(idVendedor);
    }

    public List<Vendedor> buscaVendedores() {
        return vendedorRepository.findAll();
    }
}
