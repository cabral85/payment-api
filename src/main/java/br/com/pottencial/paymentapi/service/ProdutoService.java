package br.com.pottencial.paymentapi.service;

import br.com.pottencial.paymentapi.model.Produto;
import br.com.pottencial.paymentapi.repository.ProdutoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ProdutoService {

    private final ProdutoRepository produtoRepository;

    public Optional<Produto> buscaProdutoPorId(Long idProduto) {
        return produtoRepository.findById(idProduto);
    }

    public List<Produto> listaProdutos() {
        return produtoRepository.findAll();
    }

    public boolean salvaProduto(Produto produto) {
        return produtoRepository.save(produto).getIdProduto() != null;
    }

    public boolean atualizaProduto(Long idProduto, Produto produtoAtualizado) {
        Optional<Produto> optionalProduto = produtoRepository.findById(idProduto);

        if (optionalProduto.isPresent()) {
            Produto produto = optionalProduto.get();
            BeanUtils.copyProperties(produtoAtualizado, produto, "idProduto");
            produtoRepository.save(produto);
            return true;
        }
        return false;
    }
}
